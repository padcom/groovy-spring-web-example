package org.example.web

import groovy.util.logging.Slf4j

import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.ResponseBody
import org.springframework.web.servlet.ModelAndView

@Slf4j
@Controller
public class HomeController {
	@RequestMapping("/")
	ModelAndView index() {
		return new ModelAndView("index", [ message: 'Hello, world!' ])
	}

	@RequestMapping("/data")
	@ResponseBody Map<String, Object> getCurrentRequests() {
		[ value: 3.14 ]
	}
}
