<!doctype html>

<html>
<head>
    <title>Hello, world!</title>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
    <script>
        $(document).ready(function() {
            $("#output").load("<%=request.getContextPath( )%>/data");
        });
    </script>
</head>
<body>
    <div id="output">Loading...</div>
</body>
</html>
